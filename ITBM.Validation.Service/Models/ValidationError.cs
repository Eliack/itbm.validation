﻿// <copyright file="ValidationError.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ITBM.Validation.Service.Models
{
    /// <summary>
    /// Validation Error class
    /// </summary>
    public class ValidationError
    {
        /// <summary>
        /// Gets or sets the property name.
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}