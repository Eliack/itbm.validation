﻿// <copyright file="ValidateLookUp.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ITBM.Validation.Service.Attributes
{
    using System;

    /// <summary>
    /// This class is a property to have the ValidationService validate a lookup
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateLookUp : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateLookUp"/> class.
        /// </summary>
        /// <param name="listName">This sets the name for the list lookup that is passed in the Dictionary</param>
        public ValidateLookUp(string listName)
        {
            this.ListName = listName;
        }

        /// <summary>
        /// Gets a string that is the look up of the name of the lookup
        /// </summary>
        public string ListName { get; }
    }
}