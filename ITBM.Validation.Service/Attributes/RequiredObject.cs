﻿// <copyright file="RequiredObject.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ITBM.Validation.Service.Attributes
{
    using System;

    /// <summary>
    /// The class for the RequiredObject Property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredObject : Attribute
    {
        
    }
}