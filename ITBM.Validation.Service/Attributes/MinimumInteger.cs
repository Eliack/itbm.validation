﻿// <copyright file="MinimumInteger.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ITBM.Validation.Service.Attributes
{
    using System;
    [AttributeUsage(AttributeTargets.Property)]
    public class MinimumInteger : Attribute
    {
        public MinimumInteger(int minimumLength)
        {
            this.MinimumLength = minimumLength;
        }

        public int MinimumLength { get; }
    }
}